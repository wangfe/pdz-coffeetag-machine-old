#!/usr/bin/env python



import RPi.GPIO as GPIO
import lib.MFRC522 as MFRC522
import signal
import time
import sys
from pymongo import MongoClient
from datetime import datetime

continue_reading = True
#s
# setup mongo db for uid mgmt
client = MongoClient('mongodb://localhost:27017/')
db = client.espresso
baristas = db.baristas
print "DB connected"

# function to create new barista in db
def new_barista(tag_uid):
    print "New barista..."
    first_name = raw_input("Please enter first name: ")
    name = raw_input("Please enter surname: ")
    role = raw_input("Please enter role [staff/external]: ")
    barista = {
        "name": name,
        "first_name": first_name,
        "tag_uid": tag_uid,
        "authorized": True,
        "ts": datetime.now(),
        "role": role
    }
    baristas.insert_one(barista)
    print "barista created"

# function to replace existing barista in db
def replace_barista(tag_uid):
    print "Overwrite barista..."
    first_name = raw_input("Please enter first name: ")
    name = raw_input("Please enter surname: ")
    role = raw_input("Please enter role [staff/external]: ")
    barista = {
        "name": name,
        "first_name": first_name,
        "tag_uid": tag_uid,
        "authorized": True,
        "ts": datetime.now(),
        "role": role
    }
    baristas.find_one_and_replace({"tag_uid": tag_uid}, barista, {"upsert": False})
    print "barista overwritten"

# function to delete barista in db
def delete_barista(tag_uid):
    print "Delete barista"
    baristas.find_one_and_delete({"tag_uid": tag_uid})
    print "barista deleted"


# Capture SIGINT for cleanup when the script is aborted
def end_read(signal,frame):
    global continue_reading
    print "Ctrl+C captured, ending read."
    continue_reading = False
    GPIO.cleanup()

# Hook the SIGINT
signal.signal(signal.SIGINT, end_read)

# Create an object of the class MFRC522
MIFAREReader = MFRC522.MFRC522()

# Welcome message
print "Ready to register new barista"
print "scan RFID key fob to begin"
print "Press Ctrl-C to stop."




# This loop keeps checking for chips. If one is near it will get the UID and authenticate
while continue_reading:

    # Scan for cards
    (status,TagType) = MIFAREReader.MFRC522_Request(MIFAREReader.PICC_REQIDL)

    # If a card is found
    if status == MIFAREReader.MI_OK:
        print "Tag detected"

    # Get the UID of the card
    (status,tag_uid) = MIFAREReader.MFRC522_Anticoll()

    # If we have the UID, continue
    if status == MIFAREReader.MI_OK:

        # Print UID
        print "Card read UID: %s,%s,%s,%s" % (tag_uid[0], tag_uid[1], tag_uid[2], tag_uid[3])





        print baristas.find_one({"tag_uid": tag_uid})

        if baristas.find_one({"tag_uid": tag_uid}):
            print "tag already registered"
            overwrite = raw_input("Do you want to overwrite [Y/n]? ")
            if overwrite == "Y":
                print "ok overwrite"
                replace_barista(tag_uid)
            elif overwrite == "n":
                print "dont overwrite"
                remove_barista = raw_input("Do you want to delete barista [Y/n]? ")
                if remove_barista == "Y":
                    print "ok. delete"
                    delete_barista(tag_uid)
                elif remove_barista == "n":
                    print "Ok. No changes made."
                else:
                    print "Not a valid answer. Please start process again."
            else:
                print "Not a valid answer. Please start process again."


        else:
            print "unknown tag"
            create_barista = raw_input("Do you want to register new barista [Y/n]? ")
            if create_barista == "Y":
                new_barista(tag_uid)
            else:
                print "Ok. No changes made."

        print "Done!"
        sys.exit(0)

#!/usr/bin/env python



import RPi.GPIO as GPIO
import lib.MFRC522 as MFRC522
import signal
import time
from pymongo import MongoClient
import threading

continue_reading = True
last_tag_uid = []
#s
tag_detected = False
tag_authorized = False

# Define Pins as constants (GPIO.BOARD name convetion)
GREEN_LED_PIN = 11
ORANGE_LED_PIN = 13
RED_LED_PIN = 15
RELAY_1_PIN = 18
RELAY_2_PIN = 16

# setup mongo db for uid mgmt
client = MongoClient('mongodb://localhost:27017/')
db = client.espresso
baristas = db.baristas
print "DB connected"


# Capture SIGINT for cleanup when the script is aborted
def end_read(signal,frame):
    global continue_reading
    print "Ctrl+C captured, ending read."
    continue_reading = False
    GPIO.cleanup()

# Hook the SIGINT
signal.signal(signal.SIGINT, end_read)

# Create an object of the class MFRC522
MIFAREReader = MFRC522.MFRC522()

# initialize GPIO
GPIO.setwarnings(False)
GPIO.setup(GREEN_LED_PIN,GPIO.OUT)
GPIO.setup(ORANGE_LED_PIN,GPIO.OUT)
GPIO.setup(RED_LED_PIN,GPIO.OUT)
GPIO.setup(RELAY_1_PIN,GPIO.OUT)
GPIO.setup(RELAY_2_PIN,GPIO.OUT)

# function to update relay and states accepts two bools
def update_states(tag_detected, tag_authorized):
    # print "updating states..."
    if tag_detected and tag_authorized:
        GPIO.output(ORANGE_LED_PIN,GPIO.LOW)
        GPIO.output(RED_LED_PIN,GPIO.LOW)
        GPIO.output(GREEN_LED_PIN,GPIO.HIGH)
        GPIO.output(RELAY_1_PIN,GPIO.LOW)
    elif tag_detected and not tag_authorized:
        GPIO.output(ORANGE_LED_PIN,GPIO.LOW)
        GPIO.output(RED_LED_PIN,GPIO.HIGH)
        GPIO.output(GREEN_LED_PIN,GPIO.LOW)
        GPIO.output(RELAY_1_PIN,GPIO.HIGH)
    else:
        GPIO.output(ORANGE_LED_PIN,GPIO.HIGH)
        GPIO.output(RED_LED_PIN,GPIO.LOW)
        GPIO.output(GREEN_LED_PIN,GPIO.LOW)
        GPIO.output(RELAY_1_PIN,GPIO.HIGH)


# Welcome message
print "pd|z espresso access is running"
print "Press Ctrl-C to stop."
GPIO.output(RELAY_2_PIN,GPIO.HIGH)

# This loop keeps checking for tags. If one is near it will get the UID and authenticate
while continue_reading:

    # Scan for cards
    (status,TagType) = MIFAREReader.MFRC522_Request(MIFAREReader.PICC_REQIDL)

    # Get the UID of the card
    (status,tag_uid) = MIFAREReader.MFRC522_Anticoll()

    # check for tag_uid in the last two loops
    if tag_uid or last_tag_uid:
        tag_detected = True

        # use last_tag_uid if existing to avoid reading gaps
        if tag_uid:
            querry_tag_uid = tag_uid
        elif not tag_uid and last_tag_uid:
            querry_tag_uid = last_tag_uid

        # Print UID
        print "Tag read UID: " + str(querry_tag_uid)

        #check if there is db entry for querry_tag_uid
        current_barista = baristas.find_one({"tag_uid": querry_tag_uid})

        if current_barista and current_barista["authorized"]:
            print "Hallo barista " + current_barista["first_name"]
            tag_authorized = True
        else:
            print "Not an authorized tag"
            tag_authorized = False
    else:
        tag_detected = False
        tag_authorized = False

    # keep last_tag_uid
    last_tag_uid = tag_uid

    # switch LEDs and relay
    update_states(tag_detected, tag_authorized)

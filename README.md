# pd|z Espresso Access

## Abouts

Switching the pd|z coffee grinder for RFID key fobs with permission.

## Prerequisites

- Raspberry Pi 3 B+
- RFID Reader MFRC522
- 2-Channel relais module 5V
- Python
- [Python SPI](https://github.com/lthiery/SPI-Py)
- [RC522 library](https://github.com/mxgxw/MFRC522-python)

## Usage

### Check Permissions

Reader starts automatically as systemctl service when Raspberry Pi has booted.

- Check status of service with `sudo systemctl status pdz-espresso-access.service`
- End service with `sudo systemctl stop pdz-espresso-access.service`
- Start service with `sudo systemctl start pdz-espresso-access.service`

### Set Permissions (with tag)

1. Connect to wifi `espresso`
2. Connect to Raspberry Pi using SSH `ssh pi@10.0.0.1` (You need a password to connect.)
3. Run authorize script: `./pdz-espresso-access/manage.sh`
4. Scan RFID Tag and follow instructions

Note: `manage.sh` terminates pdz-espresso-access.service and restarts it when done.

### Remove Permission (without tag)

In case a tag got lost or stolen you can access MongoDB:

1. Connect to wifi `espresso`
2. Connect to Raspberry Pi using SSH `ssh administrator@10.0.0.1`
3. Start MongoDB shell: `mongo` (MongoDB shell version: 2.4.14)
4. Type `use espresso`
5. Delete a barista by name. E.g. `db.baristas.remove({"name": "Fontana"})`. To delete all baristas use `db.baristas.drop()`.

The barista documents have following form:

```
  {
    "_id" : ObjectId("5c46176d74fece048ba4b752"),
    "tag_uid" : [  197,  19,  22,  119,  183 ],
    "first_name" : "Timon",
    "name" : "Heinis",
    "authorized" : true
  }
```
